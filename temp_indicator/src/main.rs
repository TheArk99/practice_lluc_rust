//sources:
//https://hermanradtke.com/2015/09/21/get-data-from-a-url-rust.html/
//https://docs.rs/regex/latest/regex/
//https://docs.rs/curl/latest/curl/
//https://dev.to/carlyraejepsenstan/regular-expressions-in-rust-5c1c
//https://stackoverflow.com/questions/1628088/reset-local-repository-branch-to-be-just-like-remote-repository-head
//https://docs.rs/regex/latest/regex/struct.Regex.html
//https://github.com/alexcrichton/curl-rust/issues/117
//https://www.linuxjournal.com/content/text-processing-rust


//use regex::Regex;

fn main() {
    use std::io::{stdout, Write};
    use curl::easy::Easy;

    let mut easy = Easy::new();
    easy.url("http://151.112.71.51/getData.json").unwrap();
    easy.write_function(|data| {
        stdout().write_all(data).unwrap();
        Ok(data.len())
    }).unwrap();
//    easy.perform().unwrap();

    let mut output: String = String::new();
    {
        let mut transfer = easy.transfer();
        transfer.write_function(|data| {
            output = String::from_utf8(Vec::from(data)).unwrap();
            Ok(data.len())
        }).unwrap();

        transfer.perform().unwrap();
    }

    if output.contains("tempc") {
        let location = output.find("tempc");
        let get_string = output.get(location);
    }

}
