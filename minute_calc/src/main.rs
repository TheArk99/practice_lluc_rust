use std::env::{args, Args};

fn main(){

    //brings in args
    let mut args: Args = args();

    //makes vars
    let hours;
    let mins;
    let secs;

    //test if args has 3 args if not do regular user input else do args
    if args.len() <= 3 || args.len() >= 5 {

        //geting user input
        let mut line = String::new();
        println!("Enter your hours:miniuts:seconds... ");
        std::io::stdin().read_line(&mut line).unwrap();
        //spliting string to get hours minutes seconds as different entries in this var
        let h_m_s: Vec<&str> = line.split(':').collect();

        //convert array to seperate vars
        let hours_str = h_m_s[0].to_string();
        let mins_str = h_m_s[1].to_string();
        let sec_str = h_m_s[2].to_string();

        //trim the last entry to del trailing white space
        let sec_str_trim = sec_str.trim();

        //convert str to int
        hours = match hours_str.parse::<f32>() {
            Ok(hours) => hours,
            Err(_) => {
                println!("Invalid input for hours");
                return;
            }
        };

        mins = match mins_str.parse::<f32>() {
            Ok(mins) => mins,
            Err(_) => {
                println!("Invalid input for minutes");
                return;
            }
        };

        secs = match sec_str_trim.parse::<f32>() {
            Ok(secs) => secs,
            Err(_) => {
                println!("Invalid input for seconds");
                return;
            }
        };
    } else {

        //assing the 1st arg to var
        let first = args.nth(1).unwrap();

        //second and third args to vars
        let second = args.nth(0).unwrap();
        let third = args.nth(0).unwrap();

        //parsing to f32
        hours = first.parse::<f32>().unwrap();
        mins = second.parse::<f32>().unwrap();
        secs = third.parse::<f32>().unwrap();
    }

    //now converting to minutes nearing the two decimal places (you know hopefully:))
    let hours_to_min = hours * 60.00;
    let secs_to_min = secs / 60.00;
    let final_result = hours_to_min + mins + secs_to_min;
    println!("Here are the minutes: {:.2}", final_result);
}
