//using these sites as refrence:
//https://www.freecodecamp.org/news/rust-in-replit/#strings-and-slices-in-rust
//https://learning-rust.github.io/docs/
//https://www.tutorialspoint.com/rust/index.htm


use std::env::{args, Args};

fn main() {
    let mut args: Args = args();
    let first = args.nth(1).unwrap();
    let second = args.nth(0).unwrap();
    let f_num = first.parse::<f32>().unwrap();
    let s_num = second.parse::<f32>().unwrap();
    let arg_real: [f32; 2]  = [f_num, s_num];
    println!("arg_real = {}, {}", arg_real[0], arg_real[1] );
}
